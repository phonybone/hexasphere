import pytest

def assert_is_ordered(tile):
    last_face = tile.faces[0]
    for face in tile.faces[1:]:
        assert last_face.is_adjacent_to(face)
        last_face = face
    assert last_face.is_adjacent_to(tile.faces[0])

def face_ids(tile):
    ids = [str(face.id) for face in tile.faces]
    return ', '.join(ids)

def test_order_faces(unit_hexagon):
    with pytest.raises(AssertionError):
        assert_is_ordered(unit_hexagon)

    unit_hexagon.order_faces()
    assert_is_ordered(unit_hexagon)

