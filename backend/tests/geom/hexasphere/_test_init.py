import json
import pytest
from collections import Counter
from hexasphere import Hexasphere

PARAMETERS = (
    (10,1,1),
    # (10,2,1),
)

@pytest.mark.parametrize('radius,n_divisions,hex_size', PARAMETERS)
def test_init(radius, n_divisions, hex_size):
    hs = Hexasphere(radius=radius, n_divisions=n_divisions, hex_size=hex_size)
    fn = f'data/hexasphere.{radius}.{n_divisions}.{hex_size}.json'
    with open(fn, 'w') as f:
        print(json.dumps(hs.to_json(), indent=4), file=f)
        print(F"{fn} written")

    print(F"{len(hs.tiles)} tiles")
    len_tiles = Counter(len(tile.boundary) for tile in hs.tiles)
    print("Tiles by boundary length:")
    # print(len_tiles)
    print(json.dumps(len_tiles, indent=4))
        
