
import * as THREE from 'three';
import { init_three, animate, tb_render_callback } from './init_scene';
import { init_soccerball } from './soccerball';
import { world } from './world';
import { sb_data } from './soccerball_data';


window.onload = function() {
    let scene = init_three();
    const mesh = init_soccerball(sb_data);
    scene.add(mesh);

    function rotate_mesh() {
	const time = Date.now() * 0.001;
	mesh.rotation.x = time * 0.25;
	mesh.rotation.y = time * 0.25;
    }

    world.updates.push(tb_render_callback);
    animate();
    console.log('yay');
}

