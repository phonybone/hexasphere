import aiofiles
import json
import pkg_resources as pr
from fastapi import APIRouter, HTTPException
from fastapi.responses import PlainTextResponse

router = APIRouter(prefix='/sphere')

@router.get("/", status_code=200)
async def get_sphere(
        response: PlainTextResponse,
        level:int = 0,          # querystring param
):
    if level == 0:
        fn = pr.resource_filename('src', 'static_files/icosahedron.json')
    elif level == 1:
        fn = pr.resource_filename('src', 'static_files/soccerball.json')
    else:
        raise HTTPException(status_code=422, detail=F"Illegal value for level: {level}")

    async with aiofiles.open(fn) as stream:
        content = await stream.read()
        icos_json = json.loads(content)
    response.headers.update({'Content-type': 'application/json'})
    return icos_json
