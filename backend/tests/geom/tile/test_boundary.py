'''
'''
from src.geom.utils import almost_equal

def test_boundary5(origin, unit_pentagon):
    _test_boundary(origin, unit_pentagon)

def test_boundary6(origin, unit_hexagon):
    _test_boundary(origin, unit_hexagon)

def _test_boundary(origin, unit_thing):
    print()
    boundary = unit_thing.boundary()
    for idx, pt in enumerate(boundary):
        print(F"[{idx}]: {pt}")

    assert len(boundary) == len(unit_thing.faces) + 1

    # check distances:
    d = boundary[0].distance(boundary[1])
    # print(F"d: {d:.4f}")
    last_pt = boundary[1]
    for pt in boundary[2:]:
        d2 = last_pt.distance(pt)
        # print(F"d2: {d2:.4f}")
        assert almost_equal(d, d2)
        last_pt = pt

    assert(boundary[0] is boundary[-1])
