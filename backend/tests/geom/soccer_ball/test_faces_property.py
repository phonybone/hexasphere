from collections import Counter
from src.geom.face import Face

def test_faces_property(sball1):
    counts = Counter(len(tile.faces) for tile in sball1.tiles.values())

    for face in sball1.faces:
        assert isinstance(face, Face)

    n_faces = len(sball1.faces)
    n_expected = sum(key*value for key, value in counts.items())
    assert n_faces == n_expected
