import os
from pydantic import BaseSettings


class Settings(BaseSettings):
    DB_URL: str = os.environ['DB_URL']

settings = Settings()

# @lru_cache
# def get_settings():
#     return Settings()
