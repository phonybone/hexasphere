from src.geom.point import Point

def test_subdivide_point():
    p1 = Point(0, 0, 0)
    p2 = Point(0, 0, 3)

    segments = p1.subdivide(p2, 2)
    for s in segments:
        print(s)
    assert len(segments) == 2
    for segment in segments:
        assert segment.x == 0
        assert segment.y == 0

    assert segments[0].z == 1
    assert segments[1].z == 2
