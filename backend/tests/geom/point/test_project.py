from src.geom.icosahedron import Icosahedron

def test_project():
    icos = Icosahedron(20)
    new_radius = 10
    for idx, corner in enumerate(icos.corners):
        corner.project(new_radius)
        d0 = corner.vector.magnitude
        assert d0 == new_radius
