from typing import List
from pydantic import BaseModel


class InputPoint(BaseModel):
    ''' simple Point '''
    x: float
    y: float
    z: float


# class InputPoints(BaseModel):
#     ''' list of InputPoints '''
#     points: List[InputPoint]
