from src.geom.face import Face
from src.geom.point import Point

def test_is_adjacent_to():
    p1 = Point(1, 2, 3)
    p2 = Point(2, 3, 4)
    p3 = Point(3, 4, 5)
    p4 = Point(4, 5, 6)
    p5 = Point(5, 6, 7)

    face1 = Face(p1, p2, p3)
    face2 = Face(p2, p3, p4)

    assert face1.is_adjacent_to(face2)
    assert face2.is_adjacent_to(face1)

    face3 = Face(p1, p4, p5)
    assert not face1.is_adjacent_to(face3)
    assert not face3.is_adjacent_to(face1)
    assert not face2.is_adjacent_to(face3)
    assert not face3.is_adjacent_to(face2)
