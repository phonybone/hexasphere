import sqlalchemy as sa
from src.config import settings
from src.logger import logger

db_url = settings.DB_URL
engine = sa.create_engine(db_url)
logger.info(F"connected to {db_url}")
