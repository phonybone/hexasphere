import math
from src.geom.utils import bracket, json2

class Point:
    '''
    Simple 3D point, and some operations
    '''
    def __init__(self, x, y, z):
        self.__x = float(x)
        self.__y = float(y)
        self.__z = float(z)

    # try to make x, y, z immutable:
    @property
    def x(self):
        return self.__x

    @property
    def y(self):
        return self.__y

    @property
    def z(self):
        return self.__z

    def __str__(self):
        return F"({self.x:.2f}, {self.y:.2f}, {self.z:.2f})"

    def __repr__(self):
        return F"Point<0x{id(self)}> ({self.x:.2f}, {self.y:.2f}, {self.z:.2f})"

    def __hash__(self):
        return hash((self.x, self.y, self.z))

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y and self.z == other.z

    def as_json(self):
        return {'x': self.x, 'y': self.y, 'z': self.z}

    def clone(self):
        return Point(self.x, self.y, self.z)

    def subdivide(self, p2, count):
        '''
        Return an array of count points equidistant between self
        and p2, but not including self and p2.  EG, p1(p2, 2)
        returns 2 points each 1/3 the distance between (p1, p2):

        P1                           P2
        0----------------------------0

        returns:
        ----------0----------0--------
        '''
        segments = []
        n = count+1
        for i in range(1, count+1):
            np = Point(
                self.x * (1-(i/n)) + p2.x * (i/n),
                self.y * (1-(i/n)) + p2.y * (i/n),
                self.z * (1-(i/n)) + p2.z * (i/n))
            segments.append(np)
        return segments

    def segment(self, point, percent):
        ''' 
        return a point on the line between self and `point`, `percent`
        distance from self.
        '''
        percent = bracket(percent, 0.01, 1.0)
        x = point.x * (1-percent) + self.x * percent
        y = point.y * (1-percent) + self.y * percent
        z = point.z * (1-percent) + self.z * percent
        return Point(x, y, z)

    def midpoint(self, point):
        return self.segment(point, 0.5)

    def project(self, radius, percent=1.0):
        ''' "push" a point along its vector to a distance of radius * percent '''
        percent = bracket(percent, 0.0, 1.0)
        mag = math.sqrt(math.pow(self.x, 2) + math.pow(self.y, 2) + math.pow(self.z, 2))
        ratio = radius * percent / mag

        return self.__class__(self.x * ratio, self.y * ratio, self.z * ratio)

    def distance(self, other):
        ''' return the distance to another point '''
        dx = self.x - other.x
        dy = self.y - other.y
        dz = self.z - other.z
        return math.sqrt(dx*dx + dy*dy + dz*dz)

if __name__ == '__main__':
    p0 = Point(1, 2, 3)
    print(json2(p0))
