from src.geom.face import Face
from src.geom.point import Point

def test_get_ordered_faces():
    return True
    p1 = Point(1,2,3)
    p2 = Point(2,3,4)
    p3 = Point(3,4,5)
    p4 = Point(4,5,6)
    p5 = Point(5,6,7)

    f1 = Face(p1, p2, p3)
    f2 = Face(p2, p3, p4)
    f3 = Face(p3, p4, p5)
    f1.register()
    f2.register()
    f3.register()

    faces = p3.get_ordered_faces()
    print()
    print(F"got {len(faces)} faces")
    assert len(faces) == 3
    assert f1 in faces
    assert f2 in faces
    assert f3 in faces

    for face in faces:
        print(F"{p1}: {face!r}")

    faces = p2.get_ordered_faces()
    assert len(faces) == 2
    assert f1 in faces
    assert f2 in faces

def test_get_ordered_faces_bad():
    return True
    p1 = Point(1,2,3)
    p2 = Point(4,5,6)
    p3 = Point(7,8,9)
    p4 = Point(10,11,12)
    p5 = Point(13,14,15)

    f1 = Face(p1, p2, p3)
    f2 = Face(p3, p4, p5)
    f1.register()
    f2.register()

    try:
        p3.get_ordered_faces()
        assert(False)
    except ValueError as e:
        print(F"caught {type(e)}: {e}")
        assert True
