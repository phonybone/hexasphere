from collections import Counter
import json
from src.geom.icosahedron import Icosahedron
from src.geom.soccer_ball import SoccerBall
from src.geom.utils import json2

def test_json():
    sb = SoccerBall(Icosahedron())
    # print(json2(sb))
    data = json.loads(json2(sb))
    assert isinstance(data, dict)
    assert 'corners' in data
    assert len(data['corners']) == 12

    assert 'tiles' in data
    assert len(data['tiles']) == 32

    faces_per_tile = Counter(len(tile['faces']) for tile in data['tiles'])
    assert len(faces_per_tile) == 2
    assert faces_per_tile[5] == 12
    assert faces_per_tile[6] == 20

    n_faces = sum(len(tile['faces']) for tile in data['tiles'])
    assert n_faces == 180       # same as sum(faces_per_tile), really

    
