from src.geom.face import Face
from src.geom.point import Point

def test_get_other_points():
    p1 = Point(1, 2, 3)
    p2 = Point(2, 3, 4)
    p3 = Point(3, 4, 5)
    face = Face(p1, p2, p3)
    assert face.get_other_points(p1) == [p2, p3]
    assert face.get_other_points(p2) == [p1, p3]
    assert face.get_other_points(p3) == [p1, p2]
    
