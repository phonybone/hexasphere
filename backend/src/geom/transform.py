'''
'''
from math import sin, cos, radians
import numpy as np
from numpy.linalg import norm

np.set_printoptions(precision=4)

class Transform:
    @classmethod
    def rotx(cls, theta):
        this = np.zeros(16).reshape(4,4)
        this[0][0] = 1.0
        this[1][1] = cos(theta)
        this[1][2] = sin(theta)
        this[2][1] = -sin(theta)
        this[2][2] = cos(theta)
        this[3][3] = 1.0
        return this

    @classmethod
    def roty(cls, theta):
        this = np.zeros(16).reshape(4,4)
        this[0][0] = cos(theta)
        this[0][2] = -sin(theta)
        this[1][1] = 1.0
        this[2][0] = sin(theta)
        this[2][2] = cos(theta)
        this[3][3] = 1.0
        return this
        
    @classmethod
    def rotz(cls, theta):
        this = np.zeros(16).reshape(4,4)
        this[0][0] = cos(theta)
        this[0][1] = sin(theta)
        this[1][0] = -sin(theta)
        this[1][1] = cos(theta)
        this[2][2] = 1.0
        this[3][3] = 1.0
        return this

if __name__ == '__main__':
    v = np.array([1.0, 0.0, 0.0, 1.0])  # unit vector in x direction

    # X
    Rx = Transform.rotx(radians(45))
    v2 = np.matmul(Rx, v)
    v2[3] = 0
    print(F"X v2:\n{v2} norm={norm(v2)}")
    assert v2[0] == 1.0
    assert v2[1] == 0.0
    assert v2[2] == 0.0
    assert norm(v2) == 1.0

    # Y
    Ry = Transform.roty(radians(45))
    v2 = np.matmul(Ry, v)
    v2[3] = 0
    print(F"Y v2:\n{v2} norm={norm(v2)}")
    assert abs(v2[0] - 0.707) < 0.001
    assert v2[1] == 0.0
    assert abs(v2[2] - 0.707) < 0.001
    assert norm(v2) == 1.0

    # Z
    Rz = Transform.rotz(radians(45))
    v2 = np.matmul(Rz, v)
    v2[3] = 0
    print(F"Z v2:\n{v2} norm={norm(v2)}")
    assert abs(v2[0] - 0.707) < 0.001
    assert abs(v2[1] + 0.707) < 0.001
    assert v2[2] == 0.0
    assert norm(v2) == 1.0

    print('yay')
