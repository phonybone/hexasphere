from functools import cached_property
from src.geom.vector import Vector

class Face:
    '''
    Face represents a three-point entity; 1+ faces can make up a Tile.
    '''
    FACECOUNT = 0

    def __init__(self, p0, p1, p2):
        self.id = Face.FACECOUNT
        Face.FACECOUNT += 1

        self.p0 = p0
        self.p1 = p1
        self.p2 = p2
        self.points = (self.p0, self.p1, self.p2)
        self.__normal = None

    def __str__(self):
        return F"<Face {self.id}>"

    def __repr__(self):
        points = ', '.join(str(p) for p in self.points)
        return F"<Face {self.id}: {points}>"

    @cached_property
    def normal(self):
        ''' return the vector normal to the plane, based at p0 '''
        if self.__normal is None:
            u = Vector.from_pts(self.p0, self.p1)
            v = Vector.from_pts(self.p0, self.p2)
            self.__normal = u.cross(v).normalized()
        return self.__normal

    def subdivide(self):
        ''' divide self equally into nine similar sub-faces; return List[face] '''
        subfaces = []

        seg01 = self.p0.subdivide(self.p1, 2)
        seg12 = self.p1.subdivide(self.p2, 2)
        seg20 = self.p2.subdivide(self.p0, 2)

        # find centroid; could use any of three cross-segments:
        seg_centroid1 = seg01[0].subdivide(seg12[1], 1)
        self.centroid = centroid = seg_centroid1[0]

        # corner faces:
        subfaces.append(Face(self.p0, seg01[0], seg20[1]))
        subfaces.append(Face(self.p1, seg12[0], seg01[1]))
        subfaces.append(Face(self.p2, seg20[0], seg12[1]))

        # mid faces:
        subfaces.append(Face(seg01[0], seg01[1], centroid))
        subfaces.append(Face(seg12[0], seg12[1], centroid))
        subfaces.append(Face(seg20[0], seg20[1], centroid))

        # center faces:
        subfaces.append(Face(seg01[0], centroid, seg20[1]))
        subfaces.append(Face(seg12[0], centroid, seg01[1]))
        subfaces.append(Face(seg20[0], centroid, seg12[1]))

        return subfaces

    def get_other_points(self, p0):
        ''' given one point of Face, return other two '''
        return [p for p in self.points if p is not p0]

    def find_third_point(self, p0, p1):
        for p in self.points:
            if p is not self.points[0] and p is not self.points[1]:
                return p
        raise ValueError("no third point???")

    def common_points(self, other):
        return set(self.points).intersection(set(other.points))

    def is_adjacent_to(self, other):
        '''
        Two faces are adjacent iff they share two points in common.
        '''
        return len(self.common_points(other)) == 2

    @staticmethod
    def order_faces(faces):
        def printfaces():
            ids = ', '.join(str(f.id) for f in faces)
            print(F"order_faces({ids})")

        if len(faces) <= 1:
            return

        i = 1
        while i < len(faces):
            printfaces()
            f0 = faces[i]
            j = i + 1
            while j < len(faces):
                next_to = f0.is_adjacent_to(faces[i])
                print(F"i={i}, j={j} next_to={next_to}")
                if not next_to:
                    print(F"swapping faces[{i}] <+> faces[{j}]")
                    faces[1], faces[i] = faces[j], faces[i]
                    break
                j += 1
            i += 1

    def as_json(self):
        return {'id': self.id, 'pts': self.points, 'normal': self.normal}

if __name__ == '__main__':
    from src.geom.point import Point
    from src.geom.utils import json2
    p0 = Point(0, 0, 0)
    p1 = Point(6, 0, 0)
    p2 = Point(3, 3, 0)
    f0 = Face(p0, p1, p2)
    print(json2(f0))
