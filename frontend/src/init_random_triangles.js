/*
  Create lots of trianges, including positions, colors, normals and material.
*/
import * as THREE from 'three';

function init_random_triangles() {
    /* Create everything and return the mesh */
    const triangles = 160;
    const n = 800, n2 = n / 2;	// triangles spread in the cube
    const d = 120, d2 = d / 2;	// individual triangle size

    const geometry = new THREE.BufferGeometry();

    const positions = [];
    const normals = [];
    const colors = [];

    const color = new THREE.Color();


    const pA = new THREE.Vector3();
    const pB = new THREE.Vector3();
    const pC = new THREE.Vector3();

    const cb = new THREE.Vector3();
    const ab = new THREE.Vector3();

    for ( let i = 0; i < triangles; i ++ ) {

	// positions

	const x = Math.random() * n - n2;
	const y = Math.random() * n - n2;
	const z = Math.random() * n - n2;

	const ax = x + Math.random() * d - d2;
	const ay = y + Math.random() * d - d2;
	const az = z + Math.random() * d - d2;

	const bx = x + Math.random() * d - d2;
	const by = y + Math.random() * d - d2;
	const bz = z + Math.random() * d - d2;

	const cx = x + Math.random() * d - d2;
	const cy = y + Math.random() * d - d2;
	const cz = z + Math.random() * d - d2;

	positions.push( ax, ay, az );
	positions.push( bx, by, bz );
	positions.push( cx, cy, cz );

	// flat face normals

	pA.set( ax, ay, az );
	pB.set( bx, by, bz );
	pC.set( cx, cy, cz );

	cb.subVectors( pC, pB );
	ab.subVectors( pA, pB );
	cb.cross( ab );

	cb.normalize();

	const nx = cb.x;
	const ny = cb.y;
	const nz = cb.z;

	normals.push( nx, ny, nz );
	normals.push( nx, ny, nz );
	normals.push( nx, ny, nz );

	// colors

	const vx = ( x / n ) + 0.5;
	const vy = ( y / n ) + 0.5;
	const vz = ( z / n ) + 0.5;

	color.setRGB( vx, vy, vz );

	const alpha = Math.random();

	colors.push( color.r, color.g, color.b, alpha );
	colors.push( color.r, color.g, color.b, alpha );
	colors.push( color.r, color.g, color.b, alpha );
    }

    // positions attribute
    function disposeArray() { this.array = null; }
    const posAttrs = new THREE.Float32BufferAttribute(positions, 3).onUpload(disposeArray);
    geometry.setAttribute( 'position', posAttrs);

    // normals attribute
    const normalAttrs = new THREE.Float32BufferAttribute(normals, 3).onUpload(disposeArray);
    geometry.setAttribute( 'normal', normalAttrs);

    // colors attribute
    const colorAttrs =  new THREE.Float32BufferAttribute(colors, 4).onUpload(disposeArray);
    geometry.setAttribute( 'color', colorAttrs);

    geometry.computeBoundingSphere();

    const material = new THREE.MeshPhongMaterial( {
	color: 0xaaaaaa, specular: 0xffffff, shininess: 250,
	side: THREE.DoubleSide, vertexColors: true, transparent: true
    } );

    let mesh = new THREE.Mesh( geometry, material );
    return mesh;

}

export { init_random_triangles };
