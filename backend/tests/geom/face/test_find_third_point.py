from src.geom.face import Face
from src.geom.point import Point

def test_find_third_point():
    p1 = Point(1, 2, 3)
    p2 = Point(2, 3, 4)
    p3 = Point(3, 4, 5)
    face = Face(p1, p2, p3)
    assert face.find_third_point(p1, p2) == p3
    assert face.find_third_point(p2, p3) == p1
    assert face.find_third_point(p1, p3) == p2
