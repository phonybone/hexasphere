from collections import Counter
from src.geom.icosahedron import Icosahedron
from src.geom.tile import Tile
from src.geom.utils import json2

class SoccerBall:
    def __init__(self):   # change 'icos' to 'parent'?
        self.corners = None
        self.tiles = {}         # key=center point
        self.__faces = None

    @classmethod
    def from_icos(cls, icos):
        self = cls()
        self.corners = [pt.clone() for pt in icos.corners]

        for pt in self.corners:
            self.tiles[pt] = Tile(pt)  # t5s
            print(F"t5 @ {pt}")
        # subdivide faces & create hex tiles:
        for face in icos.faces:
            subfaces = face.subdivide()
            assert len(subfaces) == 9
            assert face.centroid is not None
            self.tiles[face.centroid] = Tile(face.centroid)
            print(F"t6 @ {face.centroid}")

            # assign subface to correct tile:
            for sf in subfaces:
                for pt in sf.points:
                    if pt in self.tiles:
                        self.tiles[pt].add_face(sf)
                        break
                else:
                    # all subfaces must be assigned to a tile:
                    raise ValueError(f"orphan subface: {sf!r}")
        return self

    @property
    def faces(self):
        if self.__faces is None:
            self.__faces = [face for tile in self.tiles.values() for face in tile.faces]
        return self.__faces

    def as_json(self):
        return {
            'corners': self.corners,
            'tiles': [t.as_json() for t in self.tiles.values()]
        }

if __name__ == '__main__':
    import sys
    try:
        radius = float(sys.argv[1])
    except IndexError:
        radius = 1.0
    icos = Icosahedron(R=radius)

    sb = SoccerBall.from_icos(icos)
    fn = F'soccerball.{int(radius)}.json'
    with open(fn, 'w') as fyle:
        print(json2(sb), file=fyle)
        print(f'{fn} written')

    # do tests:
    assert len(sb.corners) == 12
    # for pt in sb.corners:
    #     assert len(pt.faces) == 5, F"corner {id(pt)}: {len(pt.faces)} != 5"

    print(F"{len(sb.tiles)} tiles, should be 32")
    assert len(sb.tiles) == 32
    counter = Counter(len(tile.faces) for tile in sb.tiles.values())
    print(counter)
    pent_tiles = [tile for tile in sb.tiles.values() if len(tile.faces) == 5]
    assert len(pent_tiles) == 12, f"got {len(pent_tiles)} pent_tiles"

    hex_tiles = [tile for tile in sb.tiles.values() if len(tile.faces) == 6]
    assert len(hex_tiles) == 20

    # sb2 = SoccerBall()
    # fn = F'soccerball2.{int(radius)}.json'
    # with open(fn, 'w') as fyle:
    #     print(json2(sb2), file=fyle)
    #     print(F"{fn} written")

    # counter2 = Counter(len(tile.faces) for tile in sb2.tiles.values())
    # print(F"counter2: {counter2}")

    # pts = list(sb2.tiles.keys())
    # for pt in pts:
    #     print(pt)
    print('yay')
