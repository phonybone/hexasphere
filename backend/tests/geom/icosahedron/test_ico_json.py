import json
from src.geom.icosahedron import Icosahedron
from src.geom.string_encoder import StringEncoder

def test_json():
    ico = Icosahedron()
    print(json.dumps(ico, indent=2, cls=StringEncoder))
    # assert False                # force pytest to display stdout
