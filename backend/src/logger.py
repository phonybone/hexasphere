import sys
import logging

logger = logging.getLogger('hexsphere')
logger.setLevel(logging.DEBUG)

handler = logging.StreamHandler(stream=sys.stdout)
fmt = '%(levelname)s %(filename)s:%(lineno)s %(message)s'
handler.setFormatter(logging.Formatter(fmt))
logger.addHandler(handler)

