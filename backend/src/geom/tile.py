'''
'''

class Tile:
    ''' sets of adjacent Faces '''
    N = 0

    def __init__(self, center):
        self.faces = []
        self.neighbors = []
        self.center = center    # Point
        self.id = Tile.N
        Tile.N += 1

    def add_face(self, face):
        '''
        idempotent add face to tile; face must be adjacent to one other face,
        unless it's the first face.
        '''
        if self.center not in face.points:
            raise RuntimeError(F"face {face} does not contain center {self.center}")
        self.faces.append(face)

    def add_faces(self, *faces):
        for face in faces:
            self.add_face(face)

    def common_pts(self):
        ''' return the point(s) common to all faces '''
        # initialize 'points' using arbitrary element of self.faces:
        points = set(self.faces[0].points)
        for face in self.faces:
            points = points.intersection(set(face.points))
        return points

    def order_faces(self):
        ''' order faces in-place by adjacency (if possible) '''
        ordered = [self.faces.pop()]
        while self.faces:
            to_move = None
            for face in self.faces:
                if face.is_adjacent_to(ordered[-1]):
                    to_move = face
                    break
            if to_move is None:
                raise ValueError("unable to order faces")
            ordered.append(to_move)
            self.faces.remove(to_move)
        self.faces = ordered
        return self

    def boundary(self):
        self.order_faces()
        face0 = self.faces[0]
        boundary = face0.get_other_points(self.center)
        for face in self.faces[1:]:
            next_pt = face.find_third_point(boundary[-1], self.center)
            boundary.append(next_pt)

        # ensure continuity (orig order may have been wrong):
        boundary[0] = boundary[-1]
        return boundary

    def is_planar(self, tol=0.00001):
        faces = list(self.faces)
        n = faces[0].normal.normalized()
        for face in faces[1:]:
            fn = face.normal.normalized()
            if not fn.within(n, tol):
                return False
        return True

    # def add_neighbor(self, tile):
        
    def as_json(self):
        return {
            'id': self.id,
            'faces': [face for face in self.faces],
            'center': self.center
        }
