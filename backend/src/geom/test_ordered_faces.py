'''
'''
import random

def test_ordered_faces(unit_hexagon):
    random.seed(0)
    random.shuffle(unit_hexagon.faces)
    ofaces = unit_hexagon.ordered_faces()
    f0 = ofaces[0]
    for f in ofaces:
        assert f0.is_adjacent_to(f)
        f0 = f
