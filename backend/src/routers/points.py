from fastapi import APIRouter
from typing import List
import sqlalchemy as sa
from src.models import InputPoint
from src.db import engine

router = APIRouter(prefix='/points')

@router.get("/", status_code=200)
async def allpoint():
    ''' return all (filtered?) points '''
    results = engine.execute(sa.text('SELECT * FROM point')).fetchall()
    return {"points": [dict(row) for row in results]}

@router.post("/", status_code=201)
async def newpoints(points: List[InputPoint]):
    ''' create new points '''
    sql = '''CREATE TEMP TABLE tmp_point (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  x REAL NOT NULL,
  y REAL NOT NULL,
  z REAL NOT NULL
)
'''
    engine.execute(sa.text(sql))

    sql = 'INSERT INTO tmp_point (x, y, z) VALUES (:x, :y, :z)'
    values = [dict(point) for point in points]
    engine.execute(sa.text(sql), values)

    sql = 'SELECT id FROM point JOIN tmp_point ON point.id=tmp_point.id'
