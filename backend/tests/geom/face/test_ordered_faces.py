'''
'''
from src.geom.face import Face

def test_ordered_faces(unit_hexagon):
    faces = unit_hexagon.faces
    print(', '.join(str(f.id) for f in faces))
    Face.order_faces(faces)
    print(', '.join(str(f.id) for f in faces))

    f0 = faces[0]
    for f in faces:
        assert f0.is_adjacent_to(f)
        f0 = f
