import os
from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles

from src.routers import point
from src.routers import points
from src.routers import sphere

app = FastAPI()

static_dir = os.path.abspath(os.path.join(__file__,
                                          os.pardir,
                                          os.pardir,
                                          os.pardir,
                                          'frontend',
                                          'dist'))
app.mount("/static", StaticFiles(directory=str(static_dir)), name="static")

@app.get("/")
async def root():
    return {"data": "hello world"}


app.include_router(point.router)
app.include_router(points.router)
app.include_router(sphere.router)
