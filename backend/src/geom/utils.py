'''
Utilities, geometery and others.
'''
from functools import partial
import json
import math
from src.geom.string_encoder import StringEncoder

TAO = 1.61803399

json2 = partial(json.dumps, indent=2, cls=StringEncoder)

def bracket(x, mini, maxi):
    return max(mini, min(x, maxi))


def pointing_away_from_origin(p, v):
    return ((p.x * v.x) >= 0) and ((p.y * v.y) >= 0) and ((p.z * v.z) >= 0)


def almost_equal(f1, f2, epsilon=1e-9):
    return abs(f1-f2) < epsilon


def cart2sphere(x, y, z):
    ''' interpret self as cartesian coords and return the spherical equivalent (r, phi, theta) '''
    r = math.sqrt(x * x + y * y + z * z)
    if x == 0:
        theta = 0
    else:
        theta = math.atan(y / x)
    phi = math.acos(z / r)
    return r, theta, phi


def sphere2cart(r, theta, phi):
    ''' interpret self as spherical coords (r, phi, theta) and return the cartesian equivalent '''
    x = r * math.cos(theta) * math.sin(phi)
    y = r * math.sin(theta) * math.sin(phi)
    z = r * math.cos(phi)
    return x, y, z
