import math
from src.geom.icosahedron import Icosahedron

def test_to_spherical():
    print()
    icos = Icosahedron()
    for idx, corner in enumerate(icos.corners):
        print(F"cartesian{[idx]}: {corner}")
        spherical = corner.as_spherical()
        print(F"radians:      {spherical}")
        spherical.y = math.degrees(spherical.y)
        spherical.z = math.degrees(spherical.z)
        print(F"degrees:      {spherical}\n")
