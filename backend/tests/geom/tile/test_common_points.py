import math
import random
from src.geom.tile import Tile
from src.geom.face import Face
from src.geom.point import Point
from src.geom.utils import json2

def test_common_points(origin, unit_hexagon):
    cpts = unit_hexagon.common_pts()
    assert cpts == set([origin])
