import * as THREE from 'three';

function init_soccerball(sb_data) {
    /* Build a soccerball mesh */
    const geometry = new THREE.BufferGeometry();
    let positions = [];
    let normals = [];
    let colors = [];
    const color = new THREE.Color();
    function disposeArray() { this.array = null; }
    const alpha = 1.0;

    for (const tile_idx in sb_data.tiles) {
	const tile = sb_data.tiles[tile_idx];

	// set color for this tile:
	const vx = Math.random() * 0.75 + 0.25;
	const vy = Math.random() * 0.75 + 0.25;
	const vz = Math.random() * 0.75 + 0.25;
	color.setRGB(vx, vy, vz);

	for (const face_idx in tile.faces) {
	    const face = tile.faces[face_idx];
	    const pts = face.pts;
	    positions.push(pts[0].x, pts[0].y, pts[0].z);
	    positions.push(pts[1].x, pts[1].y, pts[1].z);
	    positions.push(pts[2].x, pts[2].y, pts[2].z);

	    normals.push(face.normal.x, face.normal.y, face.normal.z);
	    normals.push(face.normal.x, face.normal.y, face.normal.z);
	    normals.push(face.normal.x, face.normal.y, face.normal.z);

	    // color:
	    colors.push(color.r, color.g, color.b, alpha);
	    colors.push(color.r, color.g, color.b, alpha);
	    colors.push(color.r, color.g, color.b, alpha);
	}
    }

    // set attrs:
    const posAttrs = new THREE.Float32BufferAttribute(positions, 3).onUpload(disposeArray);
    geometry.setAttribute( 'position', posAttrs);
    const normalAttrs = new THREE.Float32BufferAttribute(normals, 3).onUpload(disposeArray);
    geometry.setAttribute( 'normal', normalAttrs);
    const colorAttrs =  new THREE.Float32BufferAttribute(colors, 4).onUpload(disposeArray);
    geometry.setAttribute( 'color', colorAttrs);

    geometry.computeBoundingSphere();

    const material = new THREE.MeshPhongMaterial( {
	color: 0xaaaaaa, specular: 0xffffff, shininess: 250,
	side: THREE.DoubleSide, vertexColors: true, transparent: true
    } );

    let mesh = new THREE.Mesh( geometry, material );
    return mesh;
}

export { init_soccerball };
