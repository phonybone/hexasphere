'''
Icosahedron primitive
'''

from src.geom.point import Point
from src.geom.face import Face

TAO = 1.61803399

class Icosahedron:
    ''' Hard-coded icosohedron'''
    def __init__(self, R=1000):
        self.radius = R
        self.corners = [             # 12 of these
            Point( 1.0,  TAO,    0).project(R),
            Point(-1.0,  TAO,    0).project(R),
            Point( 1.0, -TAO,    0).project(R),
            Point(-1.0, -TAO,    0).project(R),
            Point(   0,  1.0,  TAO).project(R),
            Point(   0, -1.0,  TAO).project(R),
            Point(   0,  1.0, -TAO).project(R),
            Point(   0, -1.0, -TAO).project(R),
            Point( TAO,    0,  1.0).project(R),
            Point(-TAO,    0,  1.0).project(R),
            Point( TAO,    0, -1.0).project(R),
            Point(-TAO,    0, -1.0).project(R)
        ]

        self.faces = [               # 20 of these
            Face(self.corners[0], self.corners[1], self.corners[4]),
            Face(self.corners[1], self.corners[9], self.corners[4]),
            Face(self.corners[4], self.corners[9], self.corners[5]),
            Face(self.corners[5], self.corners[9], self.corners[3]),
            Face(self.corners[2], self.corners[3], self.corners[7]),
            Face(self.corners[3], self.corners[2], self.corners[5]),
            Face(self.corners[7], self.corners[10], self.corners[2]),
            Face(self.corners[0], self.corners[8], self.corners[10]),
            Face(self.corners[0], self.corners[4], self.corners[8]),
            Face(self.corners[8], self.corners[2], self.corners[10]),
            Face(self.corners[8], self.corners[4], self.corners[5]),
            Face(self.corners[8], self.corners[5], self.corners[2]),
            Face(self.corners[1], self.corners[0], self.corners[6]),
            Face(self.corners[11], self.corners[1], self.corners[6]),
            Face(self.corners[3], self.corners[9], self.corners[11]),
            Face(self.corners[6], self.corners[10], self.corners[7]),
            Face(self.corners[3], self.corners[11], self.corners[7]),
            Face(self.corners[11], self.corners[6], self.corners[7]),
            Face(self.corners[6], self.corners[0], self.corners[10]),
            Face(self.corners[9], self.corners[1], self.corners[11])
        ]

    def as_json(self):
        return {'faces': {f.id:f for f in self.faces}}

if __name__ == '__main__':
    import sys
    from src.geom.utils import json2

    try:
        radius = float(sys.argv[1])
    except IndexError:
        radius = 1.0

    icos = Icosahedron(R=radius)
    print(json2(icos))
