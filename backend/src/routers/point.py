from fastapi import APIRouter, HTTPException
import sqlalchemy as sa
from src.logger import logger
from src.models import InputPoint
from src.db import engine


router = APIRouter(prefix='/point')

@router.get("/echo", status_code=200)
async def echo(x, y, z):
    ''' return a simple point object '''
    logger.info(f'/point/echo({x}, {y}, {z})')
    return {"x": x, "y": y, "z": z}


@router.get("/{point_id}", status_code=200)
async def fetchpoint(point_id: int):
    ''' get point from the db via point_id '''
    sql = "SELECT id, x, y, z FROM point WHERE id=:id"
    results = engine.execute(sa.text(sql), id=point_id).fetchone()
    if results:
        return dict(results)
    raise HTTPException(status_code=404, detail=F"no point for id={point_id}")


@router.post("/", status_code=201)
async def newpoint(point: InputPoint):
    sql = "SELECT id FROM point WHERE x=:x AND y=:y AND z=:z"
    results = engine.execute(sa.text(sql), **dict(point)).fetchall()
    if results:
        return {"point_id": results[0][0]}

    sql = "INSERT INTO point (x, y, z) VALUES (:x, :y, :z)"
    try:
        result = engine.execute(sa.text(sql), **dict(point))
        return {"point_id": result.lastrowid}
    except Exception as e:
        logger.warn(F"caught {type(e)}: {e} on sql='{sql}'")
