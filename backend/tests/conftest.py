import math
import random
from pytest import fixture
from src.geom.icosahedron import Icosahedron
from src.geom.soccer_ball import SoccerBall
from src.geom.tile import Tile
from src.geom.face import Face
from src.geom.point import Point

@fixture
def icosahedron():
    return Icosahedron()

@fixture
def sball1(icosahedron):
    return SoccerBall(icosahedron)


@fixture
def origin():
    return Point(0, 0, 0)


def unit_polygon(origin, N):
    ''' return a Tile representing a hexagon centered on the origin '''
    alpha = math.pi / float(N) * 2.0
    points = []

    for i in range(N):
        theta = alpha * i
        points.append(Point(math.cos(theta), math.sin(theta), 0))

    faces = []
    for idx, p0 in enumerate(points):
        l_idx = (idx-1) % N
        p1 = points[l_idx]
        faces.append(Face(origin, p0, p1))

    random.shuffle(faces)
    tile = Tile(origin)
    tile.add_faces(*faces)
    return tile


@fixture
def unit_pentagon(origin):
    return unit_polygon(origin, 5)


@fixture
def unit_hexagon(origin):
    return unit_polygon(origin, 6)
