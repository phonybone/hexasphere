#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
PYTHONPATH=$(realpath "${SCRIPT_DIR}/.."):${PYTHONPATH}

source $SCRIPT_DIR/env_prepare.sh

uvicorn src.app:app --reload
