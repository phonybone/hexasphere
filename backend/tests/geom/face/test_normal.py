import math as m
from src.geom.face import Face
from src.geom.point import Point
from src.geom.utils import json2

def test_normal0():
    print()
    p0 = Point(0, 0, 0)
    p1 = Point(1, 0, 0)
    p2 = Point(0, 1, 0)
    f = Face(p0, p1, p2)
    n = f.normal.normalized()
    print(n)
    assert n.x == 0
    assert n.y == 0
    assert n.z > 0

def test_normal_consistent():
    p0 = Point(0,0,0)
    p1 = Point(1,0,0)
    p2 = Point(0, m.sqrt(2)/2, m.sqrt(2)/2)

    f0 = Face(p0, p1, p2)
    f1 = Face(p1, p2, p0)
    f2 = Face(p2, p0, p1)

    f3 = Face(p2, p1, p0)
    f4 = Face(p1, p0, p2)
    f5 = Face(p0, p2, p1)
    print()
    print(F"f0.normal: {f0.normal}\n")
    print(F"f1.normal: {f1.normal}\n")
    print(F"f2.normal: {f2.normal}\n")

    assert f0.normal == f1.normal
    assert f1.normal == f2.normal
    assert f2.normal == f0.normal

    assert f3.normal == f4.normal
    assert f4.normal == f5.normal
    assert f5.normal == f3.normal
    print(F"f3.normal: {f3.normal}")

    assert f3.normal.scale(-1) == f0.normal
    print('yay')

def test_json():
    p0 = Point(0, 0, 0)
    p1 = Point(1, 0, 0)
    p2 = Point(0, 1, 0)
    f = Face(p0, p1, p2)
    print(json2(f))
    # print(json.dumps(f.as_json()))
