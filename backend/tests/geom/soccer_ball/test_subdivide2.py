from src.geom.soccer_ball import SoccerBall

def test_subdivide2(sball1):
    ''' perform two levels of subdivision '''
    sb2 = SoccerBall(sball1)
    data = sb2.as_json()
    assert len(sb2.corners) == len(data['corners'])
    assert len(sb2.tiles) == len(data['tiles'])
