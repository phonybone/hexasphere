def test_faces(icosahedron):
    n_faces = len(icosahedron.faces)
    assert n_faces == 20
