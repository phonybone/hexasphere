from point import Point
from face import Face
from tile import Tile
from utils import TAO

corners = [             # 12 of these
    Point(1000, TAO * 1000, 0),
    Point(-1000, TAO * 1000, 0),
    Point(1000, -TAO * 1000,0),
    Point(-1000, -TAO * 1000,0),
    Point(0, 1000, TAO * 1000),
    Point(0, -1000, TAO * 1000),
    Point(0, 1000, -TAO * 1000),
    Point(0, -1000, -TAO * 1000),
    Point(TAO * 1000,0,1000),
    Point(-TAO * 1000,0,1000),
    Point(TAO * 1000,0, -1000),
    Point(-TAO * 1000,0, -1000)
]


class Hexasphere:
    def __init__(self, radius, n_divisions, hex_size):
        self.radius = radius
        self.tiles = []
        self.tile_lookup = {}

        print(F"After corners: {Point.N()} points")

        faces = [               # 20 of these
            Face(corners[0], corners[1], corners[4], False),
            Face(corners[1], corners[9], corners[4], False),
            Face(corners[4], corners[9], corners[5], False),
            Face(corners[5], corners[9], corners[3], False),
            Face(corners[2], corners[3], corners[7], False),
            Face(corners[3], corners[2], corners[5], False),
            Face(corners[7], corners[10], corners[2], False),
            Face(corners[0], corners[8], corners[10], False),
            Face(corners[0], corners[4], corners[8], False),
            Face(corners[8], corners[2], corners[10], False),
            Face(corners[8], corners[4], corners[5], False),
            Face(corners[8], corners[5], corners[2], False),
            Face(corners[1], corners[0], corners[6], False),
            Face(corners[11], corners[1], corners[6], False),
            Face(corners[3], corners[9], corners[11], False),
            Face(corners[6], corners[10], corners[7], False),
            Face(corners[3], corners[11], corners[7], False),
            Face(corners[11], corners[6], corners[7], False),
            Face(corners[6], corners[0], corners[10], False),
            Face(corners[9], corners[1], corners[11], False)
        ]
        print(F"After faces: {Point.N()} points, {Face.FACECOUNT} faces")

        # subdivide faces
        new_faces = []
        for face in faces:
            prev = None
            bottom = [face.points[0]]
            left = face.points[0].subdivide(face.points[1], n_divisions)
            right = face.points[0].subdivide(face.points[2], n_divisions)

            for i in range(1, n_divisions+1):
                prev = bottom
                bottom = left[i].subdivide(right[i], i)
                for j in range(i):
                    new_face = Face(prev[j], bottom[j], bottom[j+1])
                    new_faces.append(new_face)
                    if j > 0:
                        new_faces.append(Face(prev[j-1], prev[j], bottom[j]))
            # print(F"After {face}: {Point.N()} points")
        print(F"{Face.FACECOUNT} faces total after subdivision")

        print(F"After subdivide: {Point.N()} points")
        # for corner in corners:
        #     Point.delete(corner.x, corner.y, corner.z)
        # print(F"After subdivide2: {Point.N()} points")
            
        print(F"{len(new_faces)} new_faces")
        faces = new_faces

        # project all points
        all_coords = list(Point.all_coords())
        print(F"{len(all_coords)} points")
        for coords in all_coords:
            p = Point.at(coords)
            p.project(radius)
            new_tile = Tile(p, hex_size)
            self.tiles.append(new_tile)
            self.tile_lookup[str(new_tile)] = new_tile
        print(F"At end: {Point.N()} points")

        # # resolve neighbor references now that all have been created:
        # for t in self.tiles:
        #     self.tiles[t].neighbors = self.tiles[t].neighbor_ids

    def to_json(self):
        return {
            'radius': self.radius,
            'tiles': [t.to_json() for t in self.tiles]
        }
