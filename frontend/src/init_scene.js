import * as THREE from 'three';
import { world } from './world';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';

let scene, camera, renderer, controls;

function init_three() {
    camera = init_camera();
    renderer = init_renderer();
    scene = init_scene();

    let container = document.getElementById( 'container' );
    container.appendChild( renderer.domElement );

    controls = new OrbitControls(camera, container);

    window.addEventListener( 'resize', onWindowResize );
    return scene;
}

function init_camera() {
    camera = new THREE.PerspectiveCamera( 27, window.innerWidth / window.innerHeight, 1, 3500 );
    camera.position.z = 3250;	// was 2750
    return camera
}

function init_renderer() {
    renderer = new THREE.WebGLRenderer();
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( window.innerWidth, window.innerHeight );
    renderer.outputEncoding = THREE.sRGBEncoding;
    return renderer;
}

function init_scene() {
    scene = new THREE.Scene();
    scene.background = new THREE.Color( 0x050505 );
    scene.fog = new THREE.Fog( 0x030303, 2000, 3500 );

    scene.add( new THREE.AmbientLight( 0x444444 ) );
    // scene.add( new THREE.AmbientLight( 0xffffff ) );

    const light1 = new THREE.DirectionalLight( 0xffffff, 0.5 );
    light1.position.set( 1, 1, 1 );
    scene.add( light1 );

    const light2 = new THREE.DirectionalLight( 0xffffff, 1.5 );
    light2.position.set( 0, - 1, 0 );
    scene.add( light2 );

    return scene;
}

function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize( window.innerWidth, window.innerHeight );
}

function animate() {
    requestAnimationFrame( animate );
    render();
}

function render() {
    world.updates.forEach((func, idx) => func());
    renderer.render( scene, camera );
}

function tb_render_callback() {
    controls.update();
}

export { init_three, animate, scene, camera, render, tb_render_callback };
