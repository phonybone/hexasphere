import math as m
from dataclasses import dataclass
from functools import cached_property
# import numpy as np
# from src.geom.transform import rotx, roty, rotz

@dataclass
class Vector:
    x: float
    y: float
    z: float

    dx: float
    dy: float
    dz: float

    @classmethod
    def from_pts(cls, p0, p1):
        return cls(x=p0.x, y=p0.y, z=p0.z, dx=p1.x-p0.x, dy=p1.y-p0.y, dz=p1.z-p0.z)

    def __str__(self):
        return F"Vector<{self.x:.3f}, {self.y:.3f}, {self.z:.3f}> <{self.dx:.3f}, {self.dy:.3f}, {self.dz:.3f}> "

    def dot_product(self, other):
        return sum([self.x*other.x, self.y*other.y, self.z*other.z])
    dot = dot_product

    def cross_product(self, other):
        return Vector(
            x=self.x, y=self.y, z=self.z,
            dx=self.dy * other.dz - self.dz * other.dy,
            dy=self.dz * other.dx - self.dx * other.dz,
            dz=self.dx * other.dy - self.dy * other.dx
        )
    cross = cross_product

    def translate(self, other):
        return Vector(
            x=self.x + other.x,
            y=self.y + other.y,
            z=self.z + other.z,
            dx=self.dx, dy=self.dy, dz=self.dz,
        )

    def scale(self, n):
        return Vector(
            x=self.x, y=self.y, z=self.z,
            dx=self.dx * n,
            dy=self.dy * n,
            dz=self.dz * n
        )

    @cached_property
    def magnitude(self):
        return m.sqrt((self.dx * self.dx) + (self.dy * self.dy) + (self.dz * self.dz))

    def normalized(self):       # this could be a @cached_property
        mag = self.magnitude
        return Vector(
            x=self.x, y=self.y, z=self.z,
            dx=(self.dx/mag),
            dy=(self.dy/mag),
            dz=(self.dz/mag)
        )

    @staticmethod
    def segment(v1, v2):
        ''' Return a Vector with "ends" defined by v1 & v2 '''
        return v1.translate(v2.scale(-1))

    # def trfm_to_xy(self):
    #     ''' return the transform that rotates a face to be normal to self (and its inverse) '''
    #     p = Point(self.x, self.y, self.z).as_spherical()
    #     _, theta, phi = p.x, p.y, p.z
    #     to_xy = np.matmul(rotz(-theta), roty(m.radians(90)-phi))
    #     xy_to = np.matmul(roty(phi), rotz(theta))
    #     return to_xy, xy_to

    def as_json(self):
        return {'x': self.x, 'y': self.y, 'z': self.z, 'dx': self.dx, 'dy': self.dy, 'dz': self.dz}

if __name__ == '__main__':
    u = Vector(0, 0, 0, 1, 0, 0)
    v = Vector(0, 0, 0, 0, 1, 0)
    print(u.cross(v))
    print(u.dot_product(v))

    print(F"normal: {Vector(0, 0, 0, 30, 40, 0).normalized()}")

    print(F"segment: {Vector.segment(u, v)}")
