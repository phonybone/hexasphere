import math
import pytest
from src.geom.point import Point
from src.geom.face import Face

FACES = [
    Face(Point(0, 0, 0), Point(1, 0, 0), Point(0.5, math.sqrt(3)/2, 0)),
    Face(Point(0, 1, 3), Point(1, 3, -2), Point(math.sqrt(2)/2, math.sqrt(3)/2, math.pi)),
    Face(Point(0.00, 1000.00, 1618.03),
         Point(333.33, 1206.01, 1078.69),
         Point(-333.33, 1206.01, 1078.69)),
]

@pytest.mark.parametrize('face', FACES)
def test_subdivide(face):
    print()
    subfaces = face.subdivide()
    assert len(subfaces) == 9
    assert hasattr(face, 'centroid')
    assert isinstance(face.centroid, Point)

    # test normal vectors for all sub-faces are the same
    sf0_normal = subfaces[0].normal
    for sf in subfaces:
        err_msg = F"normals: sf0: {sf0_normal} != {sf.normal}"
        assert sf.normal.within(sf0_normal, 1e-10), err_msg
        # 1e-10 seems to be the limit so far

    # assert corner pts of original face only have one subface:
    for sf in subfaces:
        sf.register()
    for pt in face.points:
        assert len(pt.faces) == 1

    # but the centroid should have 6 faces:
    assert len(face.centroid.faces) == 6

    # for sf in face.centroid.get_ordered_faces():
    #     print(sf)
